后台管理系统
============
采用Thinkphp的Rbac开源代码，优化组织结构，修改其中一些bug。

安装说明
========
    1.php需要安装php的gd库扩展
    2.支持mysql，需要将rbac下面的rbac.sql导入你所见的db库中
    3.rbac下的conf中需要配置你所需要的数据库信息
    4.如果你需要新建项目的话，可以继承通用的commonAction，里面是一些常用的方法
    （因为本人用的是php5.5）所以大家在引用传值的过程中要注意一下差异
后续
====
后期会进一步对该框架进行优化和完善